import http from "k6/http";
import encoding from "k6/encoding";
import { RequestUtil } from "./RequestUtils.js";


const OauthEndpoints = {

    oauthTokenDynamoEndPoint : (username, password) => {
        let url = RequestUtil.createK8sRequest("/oauth/token");
        let formdata = {
            grant_type:"client_credentials"
        }
        let params = { headers: { "Content-Type": "application/x-www-form-urlencoded", "Authorization": "Basic " + encoding.b64encode(username + ":" + password) } }
        let res = http.post(url, formdata, params);
        return res;
    },


    oauthExternalCaClientCrendentials : (clientId, clientSecret, scopes) => {
        let url = RequestUtil.createExternalCaRequest("auth/oauth/v2/token");
    
        let formdata = {
            grant_type:"client_credentials",
            client_id:clientId,
            client_secret:clientSecret,
            scope:scopes
        };
    
        let params = { headers: { "Content-Type": "application/x-www-form-urlencoded","Cache-Control":"no-cache" } }
        return http.post(url, formdata, params);
    },

    oauthExternalCaPassword : (params,clientId, clientSecret,user,pass,scopes) => {
        let url = RequestUtil.createExternalCaRequest("auth/oauth/v3/token");
    
        let formdata = {
            username:user,
            password:pass,
            client_id:clientId,
            client_secret:clientSecret,
            scope:scopes,
            grant_type:"password"
        };

        return http.post(url, formdata, params);
    }

    

}

export { OauthEndpoints};