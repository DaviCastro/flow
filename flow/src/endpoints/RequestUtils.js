const RequestUtil = {

    createK8sRequest: (path) => {

        if (__ENV.K8S_HOSTNAME === undefined) {
            throw new Error("You must provide env variable K8S_HOSTNAME");
        }
        return "https://" + __ENV.K8S_HOSTNAME + "/" + path;
    },
    createInternalCaRequest: (path) => {

        if (__ENV.CA_UAT_INTERNAL_HOSTNAME === undefined) {
            throw new Error("You must provide env variable CA_UAT_INTERNAL_HOSTNAME");
        }

        return "https://" + __ENV.CA_UAT_INTERNAL_HOSTNAME + "/" + path;
    },

    createExternalCaRequest: (path) => {


        if (__ENV.CA_UAT_EXTERNAL_HOSTNAME === undefined) {
            throw new Error("You must provide env variable CA_UAT_EXTERNAL_HOSTNAME");
        }

        return "https://" + __ENV.CA_UAT_EXTERNAL_HOSTNAME + "/" + path;
    },
    createBearerHeader: (headers, token) => {
        headers["Authorization"] = "Bearer " + token;

        return headers;
    },
    createJsonPayload: (payLoad) => {
        return JSON.stringify(payLoad);
    }

};

export { RequestUtil };