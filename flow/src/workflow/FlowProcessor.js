
import BaseFlowItem from "./BaseFlowItem.js";
import FlowContext  from "./FlowContext.js";
import PayloadWrapper from "./PayloadWrapper.js";

export default class FlowProcessor extends BaseFlowItem {

    constructor(flowItens) {
        super();
        this._itens = flowItens;
    }


    get itens() {
        return this._itens;
    }

    set itens(newItens) {
        this._itens = newItens;
    }

    execute(input) {
        let payloadWrapper = PayloadWrapper.wrapPayload("FLOW_INPUT", input);
        let flowContext = new FlowContext(payloadWrapper);

        return super.execute(payloadWrapper, flowContext).getRawPayload();
    }

    doExecute(input, flowContext) {


        if (Array.isArray(this._itens) && this._itens !== 0) {
            
            //Executa o primeiro item sempre com o input injetado no fluxo
            this.executeItem(this._itens[0], PayloadWrapper.wrapPayloadDefaultKey(input), flowContext);

            for (let index = 1; index < this._itens.length; index++) {

                let item = this._itens[index];

                let input = flowContext.getSuitablePayload(item.inputId);

                this.executeItem(item, input, flowContext);
            }

            // Retorna ultimo estado do payload
            return this.getSuitableFlowOutput(flowContext);
        }
        return null;
    }


    executeItem(item, input, flowContext) {

        let payloadWrapper = item.execute(input, flowContext);

        this.addPayloadEntry(flowContext, payloadWrapper);

    }

    addPayloadEntry(flowContext, payload) {
        flowContext.addPayloadEntry(payload);
    }

    getSuitableFlowOutput(flowContext) {
        if (super.outputId != null)
            return flowContext.getPayloadState(super.outputId);

        return flowContext.currentPayloadState;
    }
}
