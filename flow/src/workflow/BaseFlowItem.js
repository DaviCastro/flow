import PayloadWrapper from "./PayloadWrapper.js";
export default class BaseFlowItem {


    constructor() {
        this._outputId = null;
        this._inputId = null;
    }

    get inputId() {
        return this._inputId;
    }

    set inputId(inputId) {
        this._inputId = inputId;
    }

    get outputId() {
        return this._outputId;
    }

    set outputId(outputId) {
        this._outputId = outputId;
    }

    execute(input, flowContext) {
        let output = this.doExecute(input.getRawPayload(), flowContext);

        if (output instanceof PayloadWrapper) {
            if (this._outputId != null)
                output.setKey(this._outputId);
            return output;
        }
        return PayloadWrapper.wrapPayload(this._outputId != null ? this._outputId : this.constructor.name, output);
    }

    doExecute(input, flowContext) {
        throw new Error('You have to implement the method doExecute!');
    }

    initData(input, flowContext) {
        throw new Error('You have to implement the method initData!');

    }



}