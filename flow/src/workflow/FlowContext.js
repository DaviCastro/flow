export default class FlowContext {


	constructor(inputWrapper) {
		this._payloadMap = new Map();
		this._currentPayloadState = inputWrapper;
		this._payloadMap.set(inputWrapper.key, inputWrapper);
	}

	addPayloadEntry(inputWrapper) {
		if (inputWrapper != null) {
			this._payloadMap.set(inputWrapper.key, inputWrapper);
			this._currentPayloadState = inputWrapper;
		}
	}


	get currentPayloadState() {
		return this._currentPayloadState;
	}


	getPayloadState(state) {
		return this._payloadMap.get(state);
	}

	getRawPayloadState(state) {
		return this.getPayloadState(state).getRawPayload();
	}

	getFlowInputParam() {
		return this.getRawPayloadState("FLOW_INPUT");
	}

	getGlobalFlowInputParam() {
		return this.getRawPayloadState("FLOW_INPUT");
	}


	getSuitablePayload(inputId) {
		if (inputId != null) {
			return this.getPayloadState(inputId);
		}
		else
			return this.currentPayloadState;
	}
}


