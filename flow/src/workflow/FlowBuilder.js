import BaseFlowItem from "./BaseFlowItem.js";
import FlowProcessor from "./FlowProcessor";
export default class FlowBuilder {
    constructor() {
        this.steps = new Array();
    }

    step(step) {

        if (this.steps === undefined) {
            this.steps = new Array();
        }

        if (!step instanceof BaseFlowItem) {
            throw new Error('Not a FlowItem');
        }
        this.steps.push(step);
        return this;
    }

    build() {
        if (this.steps.length == 0) {
            throw new Error('Steps missing');
        }

        return new FlowProcessor(this.steps);
    }
}