export default class PayloadWrapper {

    constructor(key, payload) {
        this._key = key;
        this._payload = payload;
        this._defaultKey = "DEFAULT_KEY";
    }

    get key() {
        return this._key;
    }

    set key(newKey) {
        this._key = newKey;
    }

    getRawPayload() {
        return this._payload;
    }


    set payload(payload) {
        this._payload = payload;
    }


    static wrapPayloadDefaultKey(output) {
        return PayloadWrapper.wrapPayload(this._defaultKey, output);
    }

    static wrapPayload(identifier, output) {
        return new PayloadWrapper(identifier, output);
    }

}
